# README

## Purpose

This is a flexible extension that allows you to determine its behaviour by writing JavaScript directly into it.

## To build:

npm install
npm run

The folder "*extension*" will be created with the unpacked extension inside.  Load this into chrome at chrome://extensions with developer mode turned on.

## To use:

A *W* button should appear in your browsers extension actions in the top right of your browser. Pressing it will open an extension page.

Pressing "Create Snippet" will create a new snippet. Clicking on it will take you to a modal that lets you edit the name, precontent script, postcontent script, and background script.

The precontent script is a content script that executes at document_start.  The postcontent script is a content script that executes at document_end.  The background script executes in the background.

The background script will only fire in response to a message from a content script. Your background script will be embedded in this background code:

```javascript
chrome.runtime.onMessage.addListener((message, sender) => {
    chrome.storage.local.get(["snippets"], (data) => {
        if (data.snippets) {
            data.snippets.forEach((snip) => {
                try {
                    let code = `
                        const message = arguments[0];
                        const sender = arguments[1];
                        ${snip.background}// <----- YOUR CODE
                    `;
                    let func = new Function(code);
                    func(message, sender);
                } catch (e) {

                }
            })
        }
    })
})
```

The background code will be passed the message and sender of the message. For more information on content scripts and background scripts, please look at the chrome extension documentation.

*It is recommended that you write the scripts in an editor and paste them into here. The inputs are simple textareas and the popup window can close unexpectedly before saving.*