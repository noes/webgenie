import React, { useState, useCallback, useEffect } from "react";

export const SnippetContext = React.createContext();

export const SnippetProvider = ({ children }) => {

  const [snippets, setSnippets] = useState([]);

  const loadSnippets = () => {
    chrome.storage.local.get(["snippets"], (results) => {
      let snips = results.snippets || [];
      setSnippets(snips);
    });
  }

  useEffect(() => {
    loadSnippets();
  }, [])

  const setSnippetOrder = (snippets) => {
    chrome.storage.local.set({ snippets }, () => {
      setSnippets(snippets);
    });
  }

  const saveSnippet = (snip, snippets) => {
    let newSnippets = [...snippets];
    let index = newSnippets.findIndex((s) => {
      return s.id == snip.id;
    });
    if (index == -1) return;
    newSnippets[index] = snip;
    chrome.storage.local.set({ snippets: newSnippets }, () => {
      loadSnippets();
    })
  };

  const addSnippet = (snip) => {
    const makeID = () => Math.random() + "-" + Math.random();
    let newSnippets = [...snippets];
    if (snip) {
      snip.id = makeID();
      snip.active = false;
      snip.name = snip.name || "untitled";
      snip.description = snip.description || "";
      snip.precontent = snip.precontent || "";
      snip.postcontent = snip.postcontent || "";
      snip.background = snip.background || "";
      newSnippets.push(snip);
    } else {
      newSnippets.push({
        id: makeID(),
        name: "no title",
        active: false,
        description: "",
        background: "",
        precontent: "",
        postcontent: "",
      });
    }
    chrome.storage.local.set({ snippets: newSnippets}, () => {
      loadSnippets();
    })
  };

  const deleteSnippet = (snip) => {
    let newSnippets = [...snippets];
    let index  = newSnippets.findIndex((s) => s.id == snip.id);
    if (index == -1) return;
    newSnippets.splice(index, 1);
    chrome.storage.local.set({ snippets: newSnippets}, () => {
      loadSnippets();
    })
  }

  return (
    <SnippetContext.Provider value={{ 
      snippets, 
      setSnippetOrder,
      saveSnippet,
      addSnippet,
      deleteSnippet,
    }}>
      { children }
    </SnippetContext.Provider>
  )
}