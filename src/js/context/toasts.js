import React, { useState, useCallback, useEffect } from "react";

export const ToastContext = React.createContext();

export const ToastProvider = ({ children }) => {

  const [toast, setToast] = useState(null);

  const showToast = (type = "success", message = "") => {
    setToast({
      type,
      message,
      key: Math.random(),
    })
  };

  return (
    <ToastContext.Provider value={{ 
      showToast,
      toast
    }}>
      { children }
    </ToastContext.Provider>
  )
}