import React from "react";
import styled, { keyframes } from "styled-components";

export const fadeIn = keyframes`
    0% {
      transform: scale(.3);
      opacity: 1;
    }
    10% {
      transform: scale(1);
    }
    100% {
      opacity: 0; 
    }
`;

const ToastStyle = styled.div`
  position: fixed;
  top: 10px;
  left: 120px;
  z-index: 10000000;
  padding: 5px 20px;
  color: white;
  background-color: ${({type}) => type == "success" ? "green" : "red"};
  border-radius: 5px;
  max-width: 200px;
  font-weight: bold;
  animation: ${fadeIn} 3s forwards 1;
`;

const Toast = ({ type, message }) => {
  return (
    <ToastStyle type={type}>
      { message }
    </ToastStyle>
  )
}

export default Toast;