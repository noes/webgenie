import React, { useState, useContext } from "react";
import ReactDOM from "react-dom";
import { HashRouter as Router } from "react-router-dom";
import styled from "styled-components";
import Routes from "./Routes";
import Button, { LabelButton } from "components/Button";
import Toast from "components/Toast";
import Header from "components/Header";
import { 
  SnippetContext, 
  SnippetProvider,
  ToastContext,
  ToastProvider,
} from "context";

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
  padding: 10px;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
`;



const App = () => {

  const {
    addSnippet,
  } = useContext(SnippetContext);

  const { toast } = useContext(ToastContext);

  const [fileKey, setFileKey] = useState(0);

  const onChangeFile = (e) => {
    let file = e.target.files?.[0];
    if (!file) return;
    let fr = new FileReader();
    fr.onload = (e) => {
      try {
        let uploadedSnippet = JSON.parse(e.target.result);
        addSnippet(uploadedSnippet);
      } catch (e) {
        console.log("error reading json");
      } finally {
        setFileKey(fileKey + 1);
      }
    };
    fr.onerror = (e) => {
      console.log(e);
      setFileKey(fileKey + 1);
    }
    fr.readAsText(file);
  }

  return (
    <Wrapper>
      <Header>
        <h2>WEB GENIE</h2>
        <Button style={{marginRight: 5}} onClick={() => addSnippet()}>Create Snippet</Button>
        <LabelButton onClick={() => {}}>
          Upload Snippet From JSON
          <input onChange={onChangeFile} key={fileKey} type="file" accept="application/json"/>
        </LabelButton>
      </Header>
      <Routes/>
      {
        toast && <Toast {...toast} />
      }
    </Wrapper>
  )
}

ReactDOM.render(
  <Router>
    <ToastProvider>
      <SnippetProvider>
        <App/>
      </SnippetProvider>
    </ToastProvider>
  </Router>,
  document.getElementById("root")
)