import React, { useContext, useState } from "react";
import { NavLink } from "react-router-dom";
import styled from "styled-components";
import Button from "components/Button";
import AreYouSure from "components/AreYouSure";
import { SnippetContext } from "context";

const SnippetWrapper = styled.div`
  width: 250px;
  flex-shrink: 0;
  max-height: 100%;
  overflow: auto;
  background: lightgrey;
`;

const Snippet = styled(NavLink)`
  display: flex;
  align-items: flex-stat;
  padding-top: 5px;
  flex-wrap: wrap;
  border-bottom: 1px solid grey;
  background-color: #eaeaea;
  text-decoration: none;
  color: black;
  * {
    color: black
  }
  button {
    outline: none;
  }
  &.active {
    background-color: white;
  }
`;

const ActiveDisplay = styled.div`
  margin-left: 10px;
  margin-top: 0;
  margin-bottom: 5px;
  width: 100%;

  .activeSign {
    color: ${({active}) => active ? "green" : "red"};
    margin-left: 10px;
  }
`
const ArrowButtons = styled.div`
  margin-bottom: 10px;
  padding-left: 10px;
`;
const ArrowButton = styled.button`
  width: 20px;
  height: 15px;
  text-align: center;
  border: none;
  background-color: transparent;
  margin-right: 5px;
  cursor: pointer;
  line-height: 0;
  border: 1px solid grey;

`;

const SnippetList = ({}) => {

  const {
    snippets,
    setSnippetOrder,
    deleteSnippet,
  } = useContext(SnippetContext);

  const [snipToDelete, setSnipToDelete] = useState(false);

  const onDelete = () => {
    deleteSnippet(snipToDelete);
    setSnipToDelete(false);
  }

  const move = (e, index, direction) => {
    e.preventDefault();
    e.stopPropagation();
    let newIndex = direction == "up" ? index - 1 : index + 1;
    if (newIndex >= snippets.length || newIndex < 0) return;
    let newSnippets = [...snippets];
    let temp = newSnippets[newIndex];
    newSnippets[newIndex] = newSnippets[index];
    newSnippets[index] = temp;
    setSnippetOrder(newSnippets);
  }

  if (snippets.length == 0) {
    return (
      <SnippetWrapper>
        <h3 style={{margin: "5px 0px 5px 5px"}}>No Snippets Exist</h3>
      </SnippetWrapper>
    )
  }
  return (
    <SnippetWrapper>
    {
      snippets.map((snip, i) => {
        return (
          <Snippet 
            key={i + snip.id} 
            to={`/snippets/${snip.id}`}>
            <Button 
              bg="transparent"
              style={{
                fontWeight: "bold", 
                flexGrow: 1, 
                textAlign: "left",
                maxWidth: "85%",
                wordBreak: "break-word"
              }}>
              {snip.name}
            </Button>
            <Button 
              bg="transparent" 
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
                setSnipToDelete(snip)
              }}
              style={{height: "fit-content"}}
            >
              🗑
            </Button>
            <ActiveDisplay active={snip.active}>
              Active: <span className="activeSign">◉</span>
            </ActiveDisplay>
            <ArrowButtons>
              <ArrowButton onClick={(e) => move(e, i, "up")}>▴</ArrowButton>
              <ArrowButton onClick={(e) => move(e, i, "down")}>▾</ArrowButton>
            </ArrowButtons>
          </Snippet>
        )
      })
    }
    {
      snipToDelete !== false &&
      <AreYouSure text="Deleting is permanent." onYes={onDelete} onNo={() => setSnipToDelete(false)}/>
    }
    </SnippetWrapper>
  )
}

export default SnippetList;