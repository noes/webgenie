import React, { useState, useEffect, useContext } from "react";
import { useHistory, useParams, NavLink, Prompt } from "react-router-dom";
import Editor from 'react-simple-code-editor';
import Button from "components/Button";
import { SnippetContext, ToastContext } from "context";
import CodeStyle from './CodeStyle';
import styled from "styled-components";
import { highlight, languages } from 'prismjs/components/prism-core';
import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-javascript';

const Detail = styled.div`
  border-left 1px solid grey;
  flex-grow: 1;
  z-index: 1;
  box-sizing: border-box;
  padding: 5px;
  top: 0;
  left: 0;
  background-color: #ececec;
  overflow-y: auto;
  input {
    width: 100%;
    margin-bottom: 5px;
    &[type="checkbox"] {
      width: fit-content;
      margin-left: 0px;
    }
  }
  label {
    display: block;
    margin: 5px 0;
  }
`;

const ScriptNav = styled(NavLink)`
  display: inline-block;
  padding: 10px;
  background-color: rgba(0,0,0,.2);
  color: black;
  border: 1px solid black;
  font-weight: bold;
  margin: 10px;
  margin-left: 0px;
  width: fit-content;
  text-decoration: none;
  &.active {
    background-color: white;
  }
`;


const SnippetDetail = () => {

  const { showToast } = useContext(ToastContext);

  const [name, setName] = useState("");
  const [active, setActive] = useState(false);
  const [background, setBackground] = useState("");
  const [precontent, setPrecontent] = useState("");
  const [postcontent, setPostcontent] = useState("");
  const [description, setDescription] = useState("");

  const [edited, setEdited] = useState(false);
  const { snippetID, snippetSection } = useParams();
  const history = useHistory();

  const { 
    snippets, 
    saveSnippet,
  } = useContext(SnippetContext);

  useEffect(() => {

    if (!snippetID) return;
    if (!snippetSection) {
      history.push(`/snippets/${snippetID}/content-end`);
      return;
    }

  }, [snippetID, snippetSection])

  useEffect(() => {
    if (!snippetID) return;

    let snip = snippets.find((s) => s.id === snippetID);

    if (!snip) {
      return;
    }

    setName(snip.name);
    setBackground(snip.background);
    setPrecontent(snip.precontent);
    setPostcontent(snip.postcontent);
    setDescription(snip.description || "");
    setActive(snip.active || false)
    setEdited(false);

  }, [snippetID, snippets]);

  const downloadSnippet = () => {
    let snippet = snippets.find((s) => s.id === snippetID);
    let snips = JSON.stringify(snippet, 2);
    let a = document.createElement("a");
    a.download = `${snippet.name.replaceAll(" ", "_")}.json`;
    let blob = new Blob([snips], {type: 'application/json'});
    let url = window.URL.createObjectURL(blob); 
    a.href = url;
    document.body.appendChild(a);
    a.click();
    a.parentNode.removeChild(a);
  };

  const onSave = () => {
    if (!name) {
      showToast("error", "Give it a name.");
      return;
    }
    saveSnippet({
      id: snippetID,
      active,
      name,
      background,
      precontent,
      postcontent,
      description, 
    }, snippets);
    showToast("success", "Success");
  }

  const onCancel = () => {
    history.push("/snippets");
  }

  useEffect(() => {
    const keyListener = (e) => {
      if (e.key == "s" && e.ctrlKey) {
        e.preventDefault();
        onSave();
      }
    }
    document.body.addEventListener("keydown", keyListener)

    return () => {
      document.body.removeEventListener("keydown", keyListener);
    }
  }, [onSave])

  const metaData = [
    {
      label: "Active",
      type: "checkbox",
      value: active,
      onChange: setActive,
    },
    {
      label: "Name",
      type: "input",
      value: name,
      onChange: setName,
    },
    {
      label: "Description",
      type: "input",
      value: description,
      onChange: setDescription,
    },
  ];

  const scripts = [
    {
      path: "content-start",
      label: "Content Script - Document Start",
      value: precontent,
      onChange: setPrecontent,
    },
    {
      path: "content-end",
      label: "Content Script - Document End",
      value: postcontent,
      onChange: setPostcontent,
    },
    {
      path: "background",
      label: "Background Script",
      value: background,
      onChange: setBackground,
    },
  ];

  const selectedScript = scripts.find(s => s.path == snippetSection);

  if (!snippetID) {
    return (
      <Detail style={{display: "flex", alignItems: "center", justifyContent: "center"}}>
        <h3 style={{margin: 0}}>Nothing is selected.</h3>
      </Detail>
    );
  }

  return (
    <Detail>
      {
        edited && 
        <Prompt 
          message={(params) => {
            return params.pathname.includes(snippetID) ? true : "You have unsaved work.";
          }}
        />
      }
      <div style={{display: "flex"}}>
        <Button style={{marginRight: 5}} onClick={onSave}>Save</Button>
        <Button onClick={downloadSnippet}>Download As JSON</Button>
        <Button style={{marginLeft: "auto"}} onClick={onCancel}>Close</Button>
      </div>
      {
        metaData.map((inp, i) => {
          return (
            <React.Fragment key={i}>
              <label> {inp.label} </label>
              {
                inp.type == "input" &&
                <input value={inp.value} 
                     onChange={(e) => inp.onChange(e.target.value)} 
                     onInput={() => setEdited(true)}
                     />
              }
              {
                inp.type == "checkbox" &&
                <input type="checkbox" 
                     checked={inp.value} onChange={() => {}}        
                     onClick={(e) => inp.onChange(e.target.checked)} 
                     onInput={() => setEdited(true)}
                     />
              }
            </React.Fragment>
          )
        })
      }
      {
        scripts.map((s) => {
          return (
            <ScriptNav 
              to={`/snippets/${snippetID}/${s.path}`}>
              { s.label }
            </ScriptNav>
          )
        })
      }
      {
        selectedScript &&
        <CodeStyle>
          <Editor
            placeholder={selectedScript.label}
            value={selectedScript.value}
            onValueChange={(code) => {
              setEdited(true);
              selectedScript.onChange(code);
            }}
            highlight={code => highlight(code, languages.js)}
            padding={10}
            style={{
              border: "1px solid black",
              fontFamily: '"Fira code", "Fira Mono", monospace',
              fontSize: 12,
              marginBottom: 10,
            }}
          />
        </CodeStyle>
      }
      <Button style={{marginRight: 5}} onClick={onSave}>Save</Button>
      <Button onClick={onCancel}>Close</Button>
    </Detail>
  )
}

export default SnippetDetail;