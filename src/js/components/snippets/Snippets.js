import React from "react";
import Columns from "components/layout/Columns";
import SnippetList from "./SnippetList";
import SnippetDetail from "./SnippetDetail";

export default () => {
  return (
    <Columns>
      <SnippetList />
      <SnippetDetail />
    </Columns>
  );
};