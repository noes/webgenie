import styled from "styled-components";

const Columns = styled.div`
  display: flex;
  flex-grow: 1;
  overflow: auto;
  background-color: lightgrey;
`;

export default Columns;