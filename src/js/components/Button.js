import React from "react";
import styled from "styled-components";

const Button = styled.button`
    font-weight: bold;
    border: none;
    padding: 5px 10px;
    cursor: pointer;
    color: ${({ color }) => color || "black"};
    background-color: ${({bg}) => bg || "rgba(0,0,0,.2)" };
    &:hover {
        opacity: .8;
    }
    font-family: helvetica;
`;

export const LabelButton = styled.label`
    font-family: helvetica;
    display: flex;
    align-items: center;
    justify-content: center;
    font-weight: bold;
    border: none;
    padding: 5px 10px;
    cursor: pointer;
    color: ${({ color }) => color || "black"};
    background-color: rgba(0,0,0,.2);
    &:hover {
        opacity: .8;
    }
    & input {
        display: none;
    }
`;

export default Button;