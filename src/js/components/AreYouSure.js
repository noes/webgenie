import React, { useRef, useEffect } from "react";
import Modal from "components/Modal";
import Button from "components/Button";

const AreYouSure = ({ text, onYes, onNo }) => {
    let yes = useRef();
    useEffect(() => {
        yes.current.focus();
    }, []);
    return (
        <Modal>
            <h3>Are you sure? {text}</h3>
            <Button ref={yes} focus style={{marginRight: 5}} onClick={onYes} color="blue">Yes</Button>
            <Button onClick={onNo}>No</Button>
        </Modal>
    )
}

export default AreYouSure;