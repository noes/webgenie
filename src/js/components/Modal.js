import React from "react";
import styled from 'styled-components';

const ModalWrap = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: rgba(0,0,0,.8);
    position: fixed;
    top: 0px;
    left: 0px;
    z-index: 1000000000;
`;

const ModalBox = styled.div`
    padding: 10px 20px;
    align-items: center;
    text-align: center;
    background-color: white;
`;

const Modal = ({ children }) => {
    return (
        <ModalWrap>
            <ModalBox>
                { children }
            </ModalBox>
        </ModalWrap>
    )
}

export default Modal;