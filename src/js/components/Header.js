import styled from "styled-components";

const Header = styled.header`
    display: flex;
    justify-content: space-between;
    padding-bottom: 5px;
    border-bottom: 1px solid grey;

    h2 {
        margin: 0px;
        text-align: left;
        flex-grow: 1;
    }
`;

export default Header;