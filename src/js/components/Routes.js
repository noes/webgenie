import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Snippets from "components/snippets/Snippets";

export default () => {
  return (
    <Switch>
      <Route path="/snippets/:snippetID?/:snippetSection?" component={Snippets}/>
      <Redirect to="/snippets" />
    </Switch>
  )
}