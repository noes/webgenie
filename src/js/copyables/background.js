// multi use object store
const objectStore = {};

const getWebGenieTab = (htmlPath) => {
  return new Promise((resolve, reject) => {
    chrome.tabs.query({url: htmlPath}, (results) => {
      if (results.length == 0) {
        return reject();
      } else {
        return resolve(results[0]);
      }
    })
  })
}


chrome.browserAction.onClicked.addListener((tab) => {
  let htmlPath = chrome.extension.getURL('webgenie.html');
  getWebGenieTab(htmlPath)
    .then(tab => {
      chrome.windows.update(tab.windowId, { focused: true });
      chrome.tabs.update(tab.id, { active: true });
    })
    .catch((e) => {
      chrome.tabs.create({ url: htmlPath });
    })
});
  

chrome.runtime.onMessage.addListener((message, sender) => {
  chrome.storage.local.get(["snippets"], (data) => {
    if (data.snippets) {
      data.snippets.forEach((snip) => {
        if (!snip.active) {
          return;
        }
        let code = `
          const message = arguments[0];
          const sender = arguments[1];
          const objectStore = arguments[2];
          ${snip.background}
        `;
        setTimeout(() => {
          try {
            let func = new Function(code);
            func(message, sender, objectStore)
          } catch (e) {
            console.log(e);
          }
        }, 0);
      })
    }
  })
});

const excludedListenables =  [
  'browserAction.onClicked',
  'runtime.onMessage',
];


// add listeners
for (let key in chrome) {
  if (typeof chrome[key] !== 'function') {
    let item = chrome[key];

    for (let subKey in item) {
      
      if (subKey.match(/^on/) && !excludedListenables.includes(subKey)) {

        const eventSignature = key + '.' + subKey;
        const eventID = Math.random() + '.' + Math.random();

        if (!item[subKey].addListener) {
          continue;
        }

        chrome[key][subKey].addListener((...args) => {

          chrome.tabs.query({}, function(tabs) {

            tabs.forEach((tab) => {

              chrome.tabs.sendMessage(tab.id, { type: "bg event", eventSignature, eventID, args });

            });

          });

        })
      }
    }
  }
}
