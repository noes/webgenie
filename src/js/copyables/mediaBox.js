const urlParams = new URLSearchParams(window.location.search);
const elementType = urlParams.get('type');
const url = urlParams.get('url').replaceAll("____", "&");

let attributes = urlParams.get('attributes');

console.log(attributes);

attributes = attributes.split('____').map((keyValues) => {
  return keyValues.split('----');
});

const element = document.createElement(elementType);


attributes.forEach((keyValue) => {
  element.setAttribute(keyValue[0], keyValue[1]);
});

document.body.appendChild(element);
element.src = url;

document.body.onmousemove = () => element.play();
