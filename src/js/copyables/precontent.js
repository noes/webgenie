chrome.storage.local.get(["snippets"], (data) => {
  if (data.snippets) {
    data.snippets.forEach((snip) => {
      if (!snip.active) {
        return;
      }
      let code = `
        try {
          ${snip.precontent};
        } catch (e) {
          console.log('error', e);
        }
      `;
      setTimeout(() => {
        const AsyncFunction = Object.getPrototypeOf(async function(){}).constructor
        let func = new AsyncFunction(code);
        func();
      }, 0);
    });
  } 
})