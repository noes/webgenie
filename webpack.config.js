var HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const Dotenv = require("dotenv-webpack");
const CopyPlugin = require("copy-webpack-plugin");
const path = require("path");

module.exports = env => ({
  mode: env.mode,
  entry: {
    "webgenie": path.resolve(__dirname, "src", "js", "webgenie.js"),
  },
  output: {
    path: path.resolve(__dirname, "extension"),
    filename: "js/[name].js",
  },
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: [
          { loader: "babel-loader" }
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: "../",
            },
          },
          { loader: "css-loader" }
        ],
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "images",
        },
      },
      {
        test: /\.(ttf|otf)$/i,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "fonts",
        },
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "css/[name].css"
    }),
    new HtmlWebpackPlugin({
      title: "",
      filename: "webgenie.html",
      template:  path.resolve(__dirname, "src", "html", "webgenie.html"),
      excludeChunks: []
    }),
    new HtmlWebpackPlugin({
      title: "",
      filename: "webgeniemedia.html",
      template:  path.resolve(__dirname, "src", "html", "webgeniemedia.html"),
      excludeChunks: ["webgenie"]
    }),
    new CopyPlugin([
      {
        from: path.join(__dirname, "src", "json"),
        to: path.resolve(__dirname, "extension"),
      },
      {
        from: path.join(__dirname, "src", "js", "copyables"),
        to: path.resolve(__dirname, "extension", "js"),
      },
    ]),
    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: false,
    }),
  ],
  resolve: {
    alias: {
      js: path.join(__dirname, "src", "js"),
      components: path.join(__dirname, "src", "js", "components"),
      context: path.join(__dirname, "src", "js", "context"),
      styles: path.join(__dirname, "src", "js", "styles"),
      css: path.join(__dirname, "src", "css"),
    }
  }
})